import React from 'react';
import styles from './min.module.scss';
const Min = ({minute}) => {

    return <div 
    className={styles.min} 
    style={{ transform:`rotate(${minute*6}deg)` }} ></div>
};

export default Min;
