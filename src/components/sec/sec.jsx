import React from 'react';
import styles from './sec.module.scss';

const Sec = ({ second }) => {
    return <div
        className={styles.sec}
        style={{ transform:`rotate(${second*6}deg)` }}
    ></div>
};

export default Sec;
