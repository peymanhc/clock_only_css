import React from 'react';
import styles from './hr.module.scss';

const Hr = ({ hour }) => {

    return <div 
    className={styles.hr} 
    style={{ transform:`rotate(${hour*6}deg)` }} ></div>
};

export default Hr;
