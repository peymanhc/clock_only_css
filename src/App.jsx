import React, { useState } from 'react';
import { Modal } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import styles from './App.module.scss';
import Sec from './components/sec/sec';
import Min from './components/min/Min';
import Hr from './components/hr/Hr';
// components


function App() {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const [second, setSecond] = useState(0);
    const [minute, setMinute] = useState(0);
    const [hour, setHour] = useState(0);

    setTimeout(() => {
        setHour(hour === 12 ? 0 : hour + 1);
    }, 3600000);

    setTimeout(() => {
        setMinute(minute === 60 ? 0 : minute + 1);
    }, 60000);

    setTimeout(() => {
        setSecond(second === 60 ? 0 : second + 1);
    }, 1000);
    return (
        <div>
            <div className={styles.clockface}>
                <h2 >This is From Peymanhc</h2>
                <Hr hour={hour} />
                <Min minute={minute} />
                <Sec second={second} />
            </div>
            <button onClick={handleShow} className={styles.gettime}> click to show time </button>

            <Modal show={show} onHide={handleClose} animation={false}>
                <Modal.Header closeButton>
                    <Modal.Title>Time is = {hour} : {minute} : {second}</Modal.Title>
                </Modal.Header>
            </Modal>
        </div>
    )
}

export default App;
